﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;

public class MathGameTests
{
    [Test(Description = "MathGame Tests", Author = "Tanet Posnririat")]
    public void MathGameTestsSimplePasses() {
        // Use the Assert class to test conditions.                
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator MathGameTestsWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        Debug.Log("RUN 0");
        yield return null;
        Debug.Log("RUN 1");
        yield return CreateHomeScreen();
        Debug.Log("RUN 2");
        yield return CreateGamePlayScreen();
        Debug.Log("END");
    }

    private IEnumerator CreateHomeScreen()
    {
        Debug.Log("CreateHomeScreen 0");
        yield return null;
        Debug.Log("CreateHomeScreen 1");
        SceneManager.LoadScene("SceneHomeScreen", LoadSceneMode.Single);        
        yield return new WaitForSeconds(5);
        Debug.Log("CreateHomeScreen 2");
    }

    private IEnumerator CreateGamePlayScreen()
    {
        Debug.Log("CreateGamePlayScreen 0");
        yield return null;
        Debug.Log("CreateGamePlayScreen 1");
        SceneManager.LoadScene("SceneGamePlay", LoadSceneMode.Single);          
        yield return new WaitForSeconds(5);             
        TestCase1();
        yield return new WaitForSeconds(5);
        TestCase2();
        yield return new WaitForSeconds(5);
        TestCase3();
        yield return new WaitForSeconds(5);
        TestCase2();
        yield return new WaitForSeconds(5);
        TestCase1();
        yield return new WaitForSeconds(5);
        TestCase2();
        yield return new WaitForSeconds(5);
        TestCase3();
        yield return new WaitForSeconds(5);
        TestCase3();       
        Debug.Log("CreateGamePlayScreen 2");     
    }

    // case answer correct.
    [Test(Description = "case answer correct.", Author = "Tanet Posnririat")]    
    public void TestCase1()
    {   
        Debug.Log("TestCase1");             
    }

    // case answer not correct.
    [Test(Description = "case answer not correct.", Author = "Tanet Posnririat")]        
    public void TestCase2()
    {
        Debug.Log("TestCase2");         
    }

    // case missing answer.
    [Test(Description = "case not answer.", Author = "Tanet Posnririat")]        
    public void TestCase3()
    {
        Debug.Log("TestCase3");             
    }
}
