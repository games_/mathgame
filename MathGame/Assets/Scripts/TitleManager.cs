﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Common;

public class TitleManager : MonoBehaviour {

	public Text 	m_lblBestScore;
	public Text 	m_lblScore;
	public Text 	m_txtBestScore;
	public Text 	m_txtScore;
	public Button 	m_btnPlayGame;
	public Button 	m_btnNewGame;

	// Use this for initialization
	void Start () 
	{
		m_lblBestScore.text = CommonSrcTxt.msg_bestscore;
		m_lblScore.text 	= CommonSrcTxt.msg_score;				
		m_txtBestScore.text = PrefabsData.GetBestScore().ToString("0");
		m_txtScore.text 	= PrefabsData.GetScore().ToString("0");
		m_btnPlayGame.onClick.AddListener(()=>{ OnPlayGame(); });
		m_btnPlayGame.gameObject.GetComponentInChildren<Text>().text = CommonSrcTxt.msg_btn_continue;

		m_btnNewGame.onClick.AddListener(()=>{ OnNewGame(); });
		m_btnNewGame.gameObject.GetComponentInChildren<Text>().text = CommonSrcTxt.msg_btn_play;
		
		m_btnPlayGame.interactable = (PrefabsData.GetBestScore() <= 0) ? false : true;		
		m_btnPlayGame.gameObject.SetActive((PrefabsData.GetBestScore() <= 0) ? false : true);
	}

	private void OnPlayGame()
	{
		UIEventLoader.Instance.OnLoadGamePlayScene();
	}

	private void OnNewGame()
	{
		PrefabsData.ClearBestScore();
		PrefabsData.ClearScore();		
		UIEventLoader.Instance.OnLoadGamePlayScene();		
	}
}
