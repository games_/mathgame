﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Common;

public class GameManager : MonoBehaviour 
{
	private static GameManager _instance;
	public static GameManager Instance {
		get { return _instance; }
	}

	public enum ESTATE 
	{
		None = 0,
		Ready,
		Playing,
		Pause,
		Ending
	}

	public enum EOperator 
	{
		Add = 0,
		Subtract,
		Multiply,
		Divide,
		MAX
	}

	public enum EHiddenPoint 
	{
		Q1 = 0,
		Q2,
		OP,
		AN,
		MAX
	}

	public float 		m_timeInSeconds 	= 60.0f;
	public int 			m_timeInQuestion 	= 5;
	public int 			m_minimunValue 		= 0;
	public int 			m_maximunValue 		= 15;
	public int 			m_amountAnwser 		= 4;
	public int 			m_addScore      	= 100;
	public int 			m_deleteScore   	= 50;
	public float 		m_deleteTime 		= 5;

	private ESTATE 		 m_state 			= ESTATE.Ready;
	private EOperator 	 m_operator			= EOperator.Add;
	private EHiddenPoint m_point			= EHiddenPoint.Q1;
	private int 		_num1;
	private int 		_num2;
	private int 		_answer;
	private int 		_sysmbolId;
	private int 		_correctAnswer;
	private string 		_symbolOperator;
	private int 		_score;

	private Color m_redColor 				= new Color(180.0f / 255.0f, 0, 4.0f / 255.0f, 1);
	private Color m_greenColor 				= new Color(17.0f / 255.0f, 171.0f / 255.0f, 9.0f / 255.0f, 1);
	
	private void Awake() 
	{
		if (_instance != null)
		{ return; }
		_instance = this;
	}

	private void Start() 
	{
		UIItem.onClickItemHandler += OnRecievedClickItem;
		UIProgressbar.onCompletedDelegate += OnPregressCompleted;
		OnPlaying();
	}

	private void OnDisable() 
	{
		UIItem.onClickItemHandler -= OnRecievedClickItem;	
		UIProgressbar.onCompletedDelegate -= OnPregressCompleted;
	}

	// Update is called once per frame
	private void Update () 
	{
		if (m_state == ESTATE.Playing) {
			UITimer.Instance.OnUpdateTimer(m_timeInSeconds);
			if (m_timeInSeconds <= 0) {
				m_timeInSeconds = 0;
				OnEndGame();
			}
			m_timeInSeconds -= Time.deltaTime;			
		}
	}

	/* ************************************************************************************************************************
	 * function login all game.
	 * ************************************************************************************************************************ */
	private void CreateQuestion()
	{
		int _randOper 	= Random.Range(0, (int)EOperator.MAX);
		m_operator 		= (EOperator)_randOper;
		_symbolOperator = string.Empty;
		_num2 			= Random.Range(m_minimunValue + 1, m_maximunValue + 1);		
		switch(m_operator)
		{
			case EOperator.Add:			// normaly create question
				_num1 	= Random.Range(m_minimunValue + 1, m_maximunValue + 1);
				_answer = _num1 + _num2;
				Debug.Assert(_num1 + _num2 == _answer, "logic not correct!");	
				break;
			case EOperator.Subtract:	// special question because answer will be more than 0.
				_num1 	= Random.Range(_num2, m_maximunValue + 1);
				_answer = _num1 - _num2;
				Debug.Assert(_num1 - _num2 == _answer, "logic not correct!");	
				break;
			case EOperator.Multiply:    // normaly create question
				_num1 	= Random.Range(m_minimunValue + 1, m_maximunValue + 1);				
				_answer = _num1 * _num2;				
				Debug.Assert(_num1 * _num2 == _answer, "logic not correct!");	
				break;
			case EOperator.Divide: 		// special question because answer will be only type integer not floating point.				
				_num1 	= Random.Range(m_minimunValue, m_maximunValue + 1);
				int _tempAnswer = _num1 * _num2;
				int _tempNum1   = 0;
				int _tempNum2	= 0;
				int _shuffle 	= Random.Range(0, 2);
				if (_shuffle == 0) {
					_tempNum1 	= _num1;
					_tempNum2 	= _num2;
				}
				else {
					_tempNum1 	= _num2;
					_tempNum2 	= _num1;
				}				
				_num1 			= _tempAnswer;
				_num2 			= _tempNum1;
				_answer 		= _tempNum2;	

				Debug.Assert(_num1 / _num2 == _answer, "logic not correct!");				
				break;
		}		
		_sysmbolId 		= _randOper;
		_symbolOperator = CommonSrcTxt.GetSymbolOperator(_randOper);	
		Debug.Log(_num1 + _symbolOperator + _num2 + " = " + _answer);						
		CreateHiddenQuestion();										//calculation disable choice answer
	}

	private void CreateHiddenQuestion()
	{
		int _randPoint = Random.Range(0, (int)EHiddenPoint.MAX);
		m_point = (EHiddenPoint)_randPoint;
		switch(m_point)
		{
			case EHiddenPoint.Q1: 	
				_correctAnswer = _num1; 			
				UIInterface.Instance.UpdateInterface (  CommonSrcTxt.msg_symbol_hidden, 
														_num2.ToString("0"), 
														_symbolOperator, 
														CommonSrcTxt.msg_symbol_equals, 
														_answer.ToString("0")
													 );	
				break;
			case EHiddenPoint.OP: 	
				_correctAnswer = _sysmbolId; 		
				UIInterface.Instance.UpdateInterface (  _num1.ToString("0"), 
														_num2.ToString("0"), 
														CommonSrcTxt.msg_symbol_hidden, 
														CommonSrcTxt.msg_symbol_equals, 
														_answer.ToString("0")
													 );
				break;
			case EHiddenPoint.Q2: 	
				_correctAnswer = _num2;				
				UIInterface.Instance.UpdateInterface (  _num1.ToString("0"), 
														CommonSrcTxt.msg_symbol_hidden, 
														_symbolOperator, 
														CommonSrcTxt.msg_symbol_equals, 
														_answer.ToString("0")
												 	 );
				break;
			case EHiddenPoint.AN: 	
				_correctAnswer = _answer;			
				UIInterface.Instance.UpdateInterface (  _num1.ToString("0"), 
														_num2.ToString("0"), 
														_symbolOperator, 
														CommonSrcTxt.msg_symbol_equals, 
														CommonSrcTxt.msg_symbol_hidden
													 );
				break;
		}
			
		CreateChoiceAnswer();										//create choice answer
	}

	private void CreateChoiceAnswer()
	{
		List<int> _temp = new List<int>();
		_temp.Add(_correctAnswer);
		for(int i = 1; i < m_amountAnwser; i++) { 
			_temp = (m_point != EHiddenPoint.OP) ? RandomValue(_temp, m_minimunValue, m_maximunValue) : RandomValue(_temp, (int)EOperator.Add, (int)EOperator.MAX); 
		}
		_temp = ShuffleList(_temp);					
		UIInterface.Instance.CreateChoice(_temp, m_point);
		UIProgressbar.Instance.SetCoolDown(m_timeInQuestion);
		UIScore.Instance.OnUpdateScore(_score, (_score >= 0) ? Color.black : m_redColor);
		UIBestScore.Instance.OnUpdateScore(PrefabsData.GetBestScore(), Color.black);
	}

	private void OnRecievedClickItem(GameObject _obj, UIItem.EItem _type, int _id) 
	{	
		if (_correctAnswer == _id) {			
			AddScore();
		}
		else {			
			DeleteScore();
		}		
	}

	private void OnPregressCompleted()
	{
		DeleteScore();				
	}

	virtual public void AddScore()
	{
		Vector2 _posRight = UIScore.Instance.GetPosition();
		UIInterface.Instance.CreateTextPopup("+"+m_addScore.ToString("0"), m_greenColor, _posRight);
		_score += m_addScore;
		PrefabsData.SaveScore(_score);				
		CreateQuestion(); 
		Debug.Log("Your Correct (*_*)!!!");
	}

	virtual public void DeleteScore()
	{
		Vector2 _posLeft = UITimer.Instance.GetPosition();
		Vector2 _posRight = UIScore.Instance.GetPosition();
		UIInterface.Instance.CreateTextPopup("-"+m_deleteScore.ToString("0"), m_redColor, _posRight);
		UIInterface.Instance.CreateTextPopup("-"+m_deleteTime.ToString("0"), m_redColor, _posLeft);
		_score -= m_deleteScore;
		PrefabsData.SaveScore(_score);
		m_timeInSeconds -= m_deleteTime;		
		if (m_timeInSeconds <= 0) {
			m_timeInSeconds = 0;
			m_state = ESTATE.Ending;
			StopAllCoroutines();
			OnExitGame();
		}
		else {
			CreateQuestion(); 
		}
		Debug.Log("Your Loss (T^T)");		
	}

	private List<int> RandomValue(List<int> _value, int _nim, int _max)
	{
		int _ran = Random.Range(_nim, _max);						//Random min to max value
		foreach(int _i in _value) {
			if (_i == _ran) return RandomValue(_value, _nim, _max);	//recursive function
		}
		_value.Add(_ran);											//add new value
		return _value;												//return the new random list
	}

	private List<int> ShuffleList(List<int> inputList)
	{
		List<int> randomList = new List<int>();

		System.Random r = new System.Random();
		int randomIndex = 0;
		while (inputList.Count > 0) {
			randomIndex = r.Next(0, inputList.Count); 				//Choose a random object in the list
			randomList.Add(inputList[randomIndex]); 				//add it to the new, random list
			inputList.RemoveAt(randomIndex); 						//remove to avoid duplicates
     	}
		return randomList; 											//return the new random list
	}


	/* ************************************************************************************************************************
	 * function event change state game.
	 * ************************************************************************************************************************ */
	private void OnPlaying()
	{
		UIBestScore.Instance.OnUpdateScore(PrefabsData.GetBestScore(), Color.black);
		_score = 0;
		PrefabsData.SaveScore(_score);
		m_state = ESTATE.Playing;
		m_timeInSeconds = 60.0f;		
		CreateQuestion();
	}

	public void OnReplay()
	{
		m_state = ESTATE.Pause;				
		m_timeInSeconds = 60.0f;		
		bool _removed = UIInterface.Instance.RemoveChoice();	
		if (_removed) { 
			_score = 0;
			PrefabsData.SaveScore(_score);
			m_state = ESTATE.Playing;			
		 	CreateQuestion(); 
		}
	}

	private void OnPauseGame()
	{
		m_state = ESTATE.Pause;
	}

	private void OnEndGame()
	{		
		OnExitGame();		
	}

	public void OnExitGame()
	{
		Debug.Log("current "+_score);		
		m_state = ESTATE.Ending;
		m_timeInSeconds = 0.0f;					
		UIEventLoader.Instance.OnLoadHomeScene();
	}
}
