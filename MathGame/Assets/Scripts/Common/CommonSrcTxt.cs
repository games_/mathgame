﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {    
	public static class CommonSrcTxt {
		public const string msg_version_build 	= "";
		public const string msg_bestscore 		= "Best Score";
		public const string msg_score 			= "Score";
		public const string msg_btn_play 		= "New Game";
		public const string msg_btn_continue 	= "Continue";
		public const string msg_symbol_hidden 	= "?";
		public const string msg_symbol_equals 	= "=";
		
		public static string GetSymbolOperator(int _symbolId)
		{			
			switch (_symbolId)
			{
				case 0: 	return "+";
				case 1: 	return "-";
				case 2: 	return "x";
				case 3: 	return "/";
				default: 	return " ";
			}
		}
	}
}
