﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour 
{
	private static UITimer _instance;
	public static UITimer Instance {
		get { return _instance; }
	}

	public Text m_txtTimer;
	public Color m_normalColor = Color.black;
	public Color m_alarmColor  = Color.red;
	
	// Use this for initialization
	private void Awake() 
	{
		if (_instance != null)
		{ return; }
		_instance = this;
	}
	
	public void OnUpdateTimer(float _time) 
	{		
		m_txtTimer.text  = Mathf.RoundToInt(_time).ToString("00");		
		m_txtTimer.color = (_time <= 5) ? m_alarmColor : m_normalColor;	
	}	

	public Vector2 GetPosition()
	{
		return GetComponent<RectTransform>().anchoredPosition;
	}
}
