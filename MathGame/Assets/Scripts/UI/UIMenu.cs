﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour 
{
	private static UIMenu _instance;
	public static UIMenu Instance {
		get { return _instance; }
	}

	public Button m_btnMenu;
	public Button m_newGame;
	public Button m_exitGame;

	private bool IsShowMenu = false;

	// Use this for initialization
	private void Awake () 
	{
		if (_instance != null) 
		{ return; }
		_instance 	= this;
		IsShowMenu 	= false;
		m_btnMenu.onClick.AddListener(() => { OnDisplayMenu(); });
		m_newGame.onClick.AddListener(() => { OnNewGame(); });
		m_exitGame.onClick.AddListener(() => { OnExitGame(); });
	}	

	private void OnDisplayMenu() 
	{
		IsShowMenu = !IsShowMenu;
		m_newGame.gameObject.SetActive(IsShowMenu);
		m_exitGame.gameObject.SetActive(IsShowMenu);		
	}

	private void OnNewGame()
	{
		OnDisplayMenu();
		GameManager.Instance.OnReplay();
	}

	private void OnExitGame()
	{
		OnDisplayMenu();
		GameManager.Instance.OnExitGame();
	}
}
