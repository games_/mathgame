﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProgressbar : MonoBehaviour 
{
	public static OnCompletedDelegate onCompletedDelegate;
	public delegate void OnCompletedDelegate();

	private static UIProgressbar _instance;
	public static UIProgressbar Instance {
		get { return _instance; }
	}
	
	public readonly float m_width 			= 580.0f;
	public readonly float m_height 			= 27.0f;

	public Text	 		  m_time;
	public RectTransform  m_border;
	public RectTransform  m_background;
	public RectTransform  m_foreground;
	
	private float 		  _timeCollDown 	= 0.0f;	
	private float 		  _time				= 0.0f;
	private bool 		  _startTime 		= false;
	private float 	 	  _width;
	private float 		  _height;

	// Use this for initialization
	private void Awake() 
	{
		if (_instance != null)
		{ return; }	
		_instance = this;
		
	} 

	// Update is called once per frame
	private void Update () 
	{
		if (_startTime) {
			if (_timeCollDown > 0) {
				_timeCollDown -= Time.deltaTime;
				_width = (m_width * _timeCollDown) / _time;						
				m_foreground.sizeDelta = new Vector2(_width, _height);
			}
			else {
				_startTime = false;
				onCompletedDelegate();
			}			 
			m_time.text = _timeCollDown.ToString("0.00");
		}
	}

	public void SetCoolDown(float _time = 5.0f)
	{		
		this._time 		= _time;
		_width  		= m_width;
		_height 		= m_height;
		_timeCollDown 	= _time;
		_startTime 		= true;
		m_time.text 	= _timeCollDown.ToString("0.00");
		m_foreground.sizeDelta = new Vector2(_width, _height);
	}
}
