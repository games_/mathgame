﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInterface : MonoBehaviour 
{
	private static UIInterface _instance;
	public static UIInterface Instance {
		get { return _instance; } 
	}

	public RectTransform m_canvas;
	public GameObject m_answerChoice;	
	public GameObject m_txtPopup;	
	public Vector2 offest 		= new Vector2(0, -100);	
	public float shiftX 		= 0;
	public float shiftY 		= 0;
	public UIItem m_number1;
	public UIItem m_number2;
	public UIItem m_operator;
	public UIItem m_symbol;
	public UIItem m_answer;

	private List<GameObject> _anwserChoice;
	
	private void Awake() 
	{
		if (_instance != null)
		{ return; }
		_instance = this;
	}

	public void UpdateInterface(string _num1, string _num2, string _oper, string _symbol, string _answer)
	{
		m_number1.SetValue( _num1 );
		m_number2.SetValue( _num2 );
		m_answer.SetValue( _answer );
		m_operator.SetValue( _oper );
		m_symbol.SetValue( _symbol );
	}

	public void CreateChoice(List<int> _choiceList, GameManager.EHiddenPoint _point) 
	{
		bool _removed = RemoveChoice();
		if (_removed) {
			_anwserChoice = new List<GameObject>();
			int _row = 0;
			for (int i=0; i < _choiceList.Count; i++) {
				float _posX = 0;
				float _posY = 0;				
				_posX = (i % 2 == 0) ? shiftX * -1 : shiftX;			
				if(i >= 2) {
					_row++;
					_posY = -shiftY;
				}
				GameObject _obj = Instantiate(m_answerChoice);
				_obj.name = "choice_" + i;
				_obj.transform.SetParent(m_canvas, false);
				_obj.GetComponent<RectTransform>().sizeDelta = new Vector2(150, 150);
				_obj.GetComponent<RectTransform>().anchoredPosition = offest + new Vector2(_posX, _posY);			
				_obj.GetComponent<UIItem>().m_txtValue.text = (_point != GameManager.EHiddenPoint.OP) ? _choiceList[i].ToString() : Common.CommonSrcTxt.GetSymbolOperator(_choiceList[i]);
				_obj.GetComponent<UIItem>().m_id = _choiceList[i];				
				_anwserChoice.Add(_obj);				
			}
		}
	}
	
	public bool RemoveChoice() 
	{	
		if (_anwserChoice == null) return true;		
		foreach (GameObject _obj in _anwserChoice) {
			Destroy(_obj);			
		}
		_anwserChoice.Clear();
		_anwserChoice = null;
		return true;
	}

	public void CreateTextPopup(string _msg, Color _color, Vector2 _pos)
	{		
		GameObject _obj = Instantiate(m_txtPopup) as GameObject;
		_obj.transform.SetParent(m_canvas, false);		
		_obj.SetActive(true);		
		_obj.GetComponent<UITextPopup>().SetPosition(_pos);
		_obj.GetComponent<UITextPopup>().SetDate(_msg, _color);
	}
}
