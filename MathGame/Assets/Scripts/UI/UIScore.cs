﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScore : MonoBehaviour 
{
	private static UIScore _instance;
	public static UIScore Instance {
		get { return _instance; }
	}

	public Text m_txtScore;

	// Use this for initialization
	private void Awake () 
	{
		if (_instance != null)
		{ return; }
		_instance = this;
	}

	public void OnUpdateScore(int _score, Color _color)
	{
		m_txtScore.text 	= _score.ToString("0");
		m_txtScore.color 	= _color;
	}

	public Vector2 GetPosition()
	{
		return GetComponent<RectTransform>().anchoredPosition;
	}
}
