﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextPopup : MonoBehaviour 
{
	public Text 	m_txtDynamic;
	public float 	m_timeAnim 		= 0.3f;	
	public float    m_timeDisplay	= 1.0f;
	public float 	m_offsetX 		= 50.0f;
	private float   _alpha 			= 1.0f;
	private float   _time 			= 0.0f;
	private bool 	isPlayAnim		= false;
	private bool 	isFadeIn		= false;
    private bool 	isFadeOut		= false;
	private Color   _color			= Color.black;
	
	// Update is called once per frame
	private void Update () 
	{
		if (isPlayAnim) {			
			_time += Time.deltaTime;			
			if (isFadeIn && _time <= m_timeAnim)
            {
                _alpha 		= _time / m_timeAnim;
				_color.a 	= _alpha;
                m_txtDynamic.color = _color;
            }
            else if(isFadeIn && _time >= m_timeDisplay)
            {
                isFadeIn  	= false;
                isFadeOut 	= true;
                _time = 0.0f;
            }

            if(isFadeOut && _time <= m_timeAnim)
            {
                _alpha 		= 1 - (_time / m_timeAnim);
				_color.a 	= _alpha;
                m_txtDynamic.color = _color;
            }
            else if (isFadeOut && _time >= (m_timeAnim / 2))
            {
                isPlayAnim 	= false;
                DestroyPopup();
            }
		}	
	}

	public void SetDate(string _msg, Color _color)
	{
		isPlayAnim 			= true;
		isFadeIn			= true;
		isFadeOut			= false;
		this._color 		= _color;
		this._color.a 		= 0;		
		m_txtDynamic.text 	= _msg;		
	}

	public void SetPosition(Vector2 _pos)
	{
		GetComponent<RectTransform>().anchoredPosition = _pos + new Vector2(m_offsetX, 0);
	}

	private void DestroyPopup()
	{
		Destroy(gameObject);
	}
}
