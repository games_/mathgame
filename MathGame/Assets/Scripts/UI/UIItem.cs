﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItem : MonoBehaviour 
{
	public static OnClickDelegate onClickItemHandler;
	public delegate void OnClickDelegate(GameObject _obj, EItem _type, int _id);
	public Color m_normalColor 	= Color.black;
	public Color m_answerColor 	= new Color(17.0f / 255.0f, 171.0f / 255.0f, 9.0f / 255.0f, 1);

	public enum EItem 
	{
		Q1 = 0,
		Q2,
		OP,
		EQ,
		AN,
		MAX
	}

	public Text 	m_txtValue;
	public EItem 	m_type 		= EItem.Q1;
	public int 		m_id		= -1;
	public bool 	IsButton 	= true;

	// Use this for initialization
	void Start () 
	{
		if (IsButton) {
			GetComponent<Button>().onClick.AddListener(()=>{ OnClickItem(); });
		}
	}

	public void SetValue(string _value) 
	{
		m_txtValue.text = _value;
		m_txtValue.color = (_value == Common.CommonSrcTxt.msg_symbol_hidden) ? m_answerColor : m_normalColor;
	}
	
	private void OnClickItem() 
	{		
		onClickItemHandler(gameObject, m_type, m_id);
	}
}
