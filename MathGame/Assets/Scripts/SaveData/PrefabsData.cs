﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsData 
{
	public static void SaveScore(int _score)
    {                
        if (_score > GetBestScore())
        {
            SaveBestScore(_score);            
        }
        PlayerPrefs.SetInt("score", _score);        
    }

    public static int GetScore()
    {
        int _score = PlayerPrefs.GetInt("score", 0);
        return _score;
    }

    public static void SaveBestScore(int _bestScore)
    {        
        PlayerPrefs.SetInt("bestscore", _bestScore);
    }

    public static int GetBestScore()
    {        
        int _score = PlayerPrefs.GetInt("bestscore", 0);    
        return _score;
    }

    public static void ClearBestScore()
    {
        if (PlayerPrefs.HasKey("bestscore"))
        { PlayerPrefs.DeleteKey("bestscore"); }        
    }

    public static void ClearScore()
    {
        if (PlayerPrefs.HasKey("score"))
        { PlayerPrefs.DeleteKey("score"); }
    }
}
